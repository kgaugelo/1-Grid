<?php

use App\Http\Controllers\ImportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', [ImportController::class, 'index'])->name('home');
    Route::get('/import/stop', [ImportController::class, 'stop'])->name('import.stop');
    Route::get('/import/start', [ImportController::class, 'start'])->name('import.start');
    Route::get('/import/cancel', [ImportController::class, 'cancel'])->name('import.cancel');
    Route::post('/import', [ImportController::class, 'import'])->name('import.store');
});
