<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Laravel File Upload</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>

<body>
    <div class="container mt-4">
        <div class="row">
            <div class="col">
                <h3 class="text-center mb-5">Upload File</h3>
                <form method="post" id="file-upload" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="file" name="file" class="form-control" id="image-input">
                        <span class="text-danger" id="image-input-error"></span>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Upload</button>
                    </div>
                </form>

                <script type="text/javascript">
                    $(document).ready(function (e) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $('#file-upload').submit(function (e) {
                            e.preventDefault();
                            var formData = new FormData(this);
                            $.ajax({
                                type: 'POST',
                                url: "{{ route('import.store')}}",
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: (response) => {
                                    if(response) {
                                        $('.success').text(response.message);
                                        this.reset();
                                    }
                                },
                                error: function (data) {
                                    console.log(data);
                                }
                            });
                        });
                    });
                </script>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                @csrf
                @if (isset($message))
                    <div class="alert alert-success">
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">File</th>
                            <th scope="col">Status</th>
                            <th scope="col">Progress</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($trackers as $index => $tracker)
                            <tr>
                                <td scope="row">{{ $index + 1 }}</td>
                                <td>{{ $tracker->file_name }}</td>
                                <td>{{ $tracker->imported == 0 ? 'Pending' : 'Complete' }}</td>
                                <td>{{ $tracker->rows_imported ."/". $tracker->total_rows }}</td>
                                <td>
                                    @if (!$tracker->imported)
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="{{ route("import.stop") }}" class="btn btn-warning">Stop</a>
                                            <a href="{{ route("import.start") }}" class="btn btn-secondary">Start</a>
                                            <a href="{{ route("import.cancel") }}" class="btn btn-danger">Cancel</a>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
