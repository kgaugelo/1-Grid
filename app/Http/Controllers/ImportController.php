<?php

namespace App\Http\Controllers;

@ini_set('upload_max_size', '64M');
@ini_set('upload_max_filesize', '10M');

use App\Http\Requests\ImportRequest;
use App\Models\Tracker;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Symfony\Component\Process\Process;

class ImportController extends Controller
{
    /**
     * @var Process
     */
    private $process;

    /**
     *
     */
    public function __construct()
    {
        $this->process = new Process(['php ../artisan import:file']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $trackers = Tracker::all();

        return view('welcome', [
            "trackers" => $trackers
        ]);
    }

    /**
     * @param ImportRequest $request
     * @return JsonResponse
     */
    public function import(ImportRequest $request): JsonResponse
    {
        $file = $request->file('file');

        try {
            $filename = md5(rand()) . '.' . $file->guessClientExtension();
            $file->move(base_path(env("IMPORT_FILE_STORAGE_PATH")), $filename);
            $tracker = Tracker::firstOrNew(['file_name' => $filename]);
            $tracker->imported = 0;
            $tracker->total_rows = 0;
            $tracker->rows_imported = 0;
            $tracker->save();
        } catch (\Exception $e) {
            return response()->json(["message" => $e->getMessage()]);
        }

        $this->process->start();
        return response()->json(["message" => "Your file is being processed", "trackers" => Tracker::all()]);
    }

    /**
     * @return JsonResponse
     */
    public function stop()
    {
        if ($this->process->isRunning()) {
            $this->process->stop(3, SIGINT);
            return response()->json(["message" => "Process stopped"]);
        } else {
            return response()->json(["message" => "Process not running"]);
        }
    }

    /**
     * @return JsonResponse
     */
    public function start()
    {
        if (!$this->process->isRunning()) {
            $this->process->start();
            return response()->json(["message" => "Process start"]);
        }
        return response()->json(["message" => "Process is running"]);
    }

    /**
     * @return JsonResponse
     */
    public function cancel(): JsonResponse
    {
        if ($this->process->isRunning()) {
            $this->process->signal(SIGKILL);
            return response()->json(["message" => "Process cancelled"]);
        }
        return response()->json(["message" => "Process not running"]);
    }
}
