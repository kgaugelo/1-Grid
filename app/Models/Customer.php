<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = ["name", "address", "checked", "description", "interest", "date_of_birth", "email", "account"];

    protected $guarded = [];

    /**
     * @return HasOne
     */
    public function credit_card(): HasOne
    {
        return $this->hasOne(Card::class);
    }
}
