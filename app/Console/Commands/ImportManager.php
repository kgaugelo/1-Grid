<?php

namespace App\Console\Commands;

use App\Models\Data;
use App\Models\Tracker;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ImportManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "import:file";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This imports file";

    /**
     * @var int
     */
    protected $chunkSize = 100;

    /**
     * @var array
     */
    protected $chunk = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tracker = Tracker::whereImported("0")
            ->orderBy("created_at", "DESC")
            ->first();

        if ($tracker) {
            $file_path = base_path(env("IMPORT_FILE_STORAGE_PATH")) . $tracker->file_name;

            if (file_exists($file_path)) {
                $data = $this->loadFileData($file_path, $tracker->rows_imported);
                $rows = array_chunk($data, $this->chunkSize);

                $counter = $tracker->rows_imported;
                foreach ($rows as $k => $row) {
                    foreach ($row as $c => $cell) {
                        $rows[$k][$c] = $cell;
                        if ($this->canSaveUser($cell["date_of_birth"])) {
                            $this->chunk[] = $cell;
                            $counter++;
                        }
                    }
                    // Save when chunk is complete
                    $this->captureData($this->chunk);

                    // Refresh table counts
                    $tracker = $tracker->fresh();
                    $tracker->rows_imported = $counter;
                    $tracker->total_rows = count($data);
                    $tracker->save();

                    // Reset $this->chunk
                    $this->chunk = [];
                }

                // File import complete
                $tracker = $tracker->fresh();
                $tracker->imported = 1;
                $tracker->save();
            }
        }
        return 0;
    }

    /**
     * @param $rows
     * @return void
     */
    private function captureData($rows)
    {
        $data = new Data();
        $data->content = json_encode($rows);
        $data->save();
    }

    /**
     * @param $path
     * @param $index
     * @return mixed
     */
    private function loadFileData($path, $index)
    {
        $data = [];
        $file = pathinfo($path);
        switch ($file["extension"]) {
            case 'json':
                $file_content = file_get_contents($path);
                $data = json_decode($file_content, true);
                break;
            case 'xml':
                break;
            case 'csv':
                break;
            default:
                break;
        }

        // For last rows_imported index
        if ($index) {
            return array_slice($data, $index);
        }
        return $data;
    }

    /**
     * @param $date
     * @return bool
     */
    private function canSaveUser($date): bool
    {
        $age = Carbon::parse($this->formatDate($date))->age;
        return in_array($age, range(18, 65)) || $age == 0;
    }

    /**
     * @param $date
     * @return mixed|string
     */
    private function formatDate($date)
    {
        if ($date) {
            if (Carbon::canBeCreatedFromFormat($date, "d/m/Y")) {
                $date = Carbon::createFromFormat("d/m/Y", $date)->format("Y-m-d");
            } elseif (Carbon::canBeCreatedFromFormat($date, "Y-m-d\TH:i:s.vP")) {
                $date = Carbon::createFromFormat("Y-m-d\TH:i:s.vP", $date)->format("Y-m-d");
            } elseif (Carbon::canBeCreatedFromFormat($date, "Y-m-d H:i:s")) {
                $date = Carbon::createFromFormat("Y-m-d H:i:s", $date)->format("Y-m-d");
            } else {
                $date = Carbon::createFromFormat("c", $date)->format("Y-m-d");
            }
        }

        return $date;
    }
}
