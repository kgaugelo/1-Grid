## Laravel Process

### Running App

#### Docker
```Docker (docker-compose up -d)```

#### Artisan
```composer install```

```php artisan serve```

```Visit: http://127.0.0.1:8000/```

### Processing 500 times larger?

1. I would Increase chunk size for mass assignment in the ```ImportManager``` file
2. Also use XMLRequest to upload larger files

### Handle XML or CSV file

1. Append file request mimes(comma seperated) with(xml,csv) in ImportController for validation 
2. Within the ```ImportManager.php``` command file, in function(```loadFileData```), add switch case to read file into array.
